import socket

class WebApp:
    def parse(self, request):
        received = request.decode()
        method = received.split(' ')[0]
        if method == 'POST':
            resource = received.split('\r\n\r\n name= ')[1]
        else:
            resource = received.split(' ')[1]
        return {'resource': resource, 'method': method}

    def process(self, parsedRequest):
        http = "200 OK"
        html = ("<html><body><h1>It works!</h1></body></html>")
        return http, html

    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((self.host, self.port))
        self.socket.listen(5)

    def accept_con(self):
        while True:
            print("Waiting for connections...")
            (client_socket, client_address) = self.socket.accept()
            request = client_socket.recv(1024)
            if not request:
                continue
            parsed_request = self.parse(request)
            http, html = self.process(parsed_request)
            response = "HTTP/1.1" + http + '\r\n\r\n' + html + '\r\n'
            client_socket.sendall(response.encode('utf-8'))
            client_socket.close()

class ContentPost(WebApp):
    def __init__(self, host, port):
        super().__init__(host, port)
        self.content_dict = {"/": "<html><body><h1>Main page</h1></body></html>", }

    def process(self, got):
        resource = got['resource']
        method = got['method']

        form = '<form method= "post">' \
                '<label> Introduce the resource to save:' \
                '<input name= "name" autocomplete="name"/>' \
                '<label>' \
                '<button>Send</button>' \
                '</form>'
        response = resource + ' ' + form
        if method == 'GET':
            if resource in self.content_dict:
                http_code = "200 OK"
                html_cont = "<html><body>The page has been already requested" + response + "</body></html>"
            else:
                http_code = "200 OK"
                html_cont = "<html><body>Page: " + response + "</body></html>"
            return (http_code, html_cont)

        elif method == "POST":
            http_code = "200 OK"
            self.content_dict[resource] = resource
            html_cont = "<html><body>The page:" + response + "</body></html>"
            return (http_code, html_cont)

if __name__ == '__main__':
    my_web_app = ContentPost('localhost', 1234)
    my_web_app.accept_con()
